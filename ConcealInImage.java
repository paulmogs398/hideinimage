import javax.imageio.*;
import java.io.File;
import java.io.FileInputStream;
import java.awt.image.BufferedImage; 
import java.awt.image.DataBufferByte;
import java.io.IOException;

public class ConcealInImage{
	
	public static void main(String[] args){
		if(args.length < 2){
			System.out.println("Please provide a \"secret file\" followed by a an image file.");
			System.exit(1);
		}else{
			System.out.println("Welcome to HideInImage... Hiding " + args[0] + " in " + args[1]);
		}
		ConcealInImage hii = new ConcealInImage(args[0],args[1]);
	}
	
	public ConcealInImage(String secretPath, String imagePath){
		
		// Load secrets file bytes
		byte[] secrets = new byte[0];
		try{
			secrets = openSecretFile(secretPath);
		} catch (Exception ioe){
			System.out.println("Failed to read secret file");
			System.exit(1);
		}
		
		// Load image file bytes
		int w = 0;
		int h = 0; 
		boolean alpha = false;
		byte[] pixels = new byte[0];
		try{			
			BufferedImage img = loadImage(imagePath);
			w = img.getWidth();
			h = img.getHeight();
			alpha = img.getAlphaRaster()!=null;
			pixels = getPixels(img);
		} catch (IOException ioe){
			System.out.println("Failed to read image file");
			System.exit(1);
		}
		
		// Convert secret byte array to array of bool bits
		boolean[] secretsBools = bytesToBits(secrets);
		
		// indication of length of secret file as bool bits
		boolean[] secretsSizeBools = fileSizeToBits(secretsBools.length);
		
		// Combine into one array secret array		
		boolean[] secretComplete = new boolean[secretsBools.length + secretsSizeBools.length];
		System.arraycopy(secretsSizeBools,0,secretComplete,0,secretsSizeBools.length);
		System.arraycopy(secretsBools,0,secretComplete,secretsSizeBools.length,secretsBools.length);
	
		// Show bits and Bytes
		System.out.println("");
		System.out.println("Bits in secret file: " + secretComplete.length);
		System.out.println("Bytes in image file: " + pixels.length);
		
		// Will it fit
		if(secretComplete.length > pixels.length){
			System.out.println("I need a bigger image to conceal the secret file.");
			System.exit(1);
		}else{
			System.out.println("Secret file can be hidden in image file.");
		}

		try{
			writeImage(concealInPixels(pixels,w,h,alpha,secretComplete));
		} catch (IOException ioe){
			System.out.println(ioe.getMessage());
			System.out.println("Couldn't write new image file");
			System.exit(1);
		}
		
	}
	
	
	
	private BufferedImage concealInPixels(byte[] pixels, int width, int height, boolean hasAlphaChannel, boolean[] secrets ){
		
		//BufferedImage to draw on
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		// Keep track of x and y positions
		int x = 0;
		int y = 0;

		// Get each pixel color and write new image
		for (int pixel = 0, i = 0; pixel < pixels.length; pixel = pixel + (hasAlphaChannel?4:3), i++, x++) {

			// Read pixel colour, if necessary ignoring alpha, byte order:  ?A (Alpha)?, B (Blue), G (Green), R (Red)
			int b = ( (int) pixels[pixel + (hasAlphaChannel?1:0)] & 255);
			int g = ( (int) pixels[pixel + (hasAlphaChannel?2:1)] & 255);
			int r = ( (int) pixels[pixel + (hasAlphaChannel?3:2)] & 255);
			
			// Work out x and y cors
			if(x == width){
				y++;
				x=0;
			}

			// Construct RGB value
			int rgb = r;
			rgb = (rgb << 8) + g;
			rgb = (rgb << 8) + b;

			// Is there secret data to conceal?
			if(i < secrets.length){

				// Tamper with last pixel to reflect bits
				String pixelBinary = Integer.toBinaryString(rgb);
	
				if(pixelBinary.charAt(pixelBinary.length()-1)=='0'){
					if(secrets[i]){
						// pixel ends in 0 but secret bit is 1
						rgb++;
					}
				} else{
					if(!secrets[i]){
						// pixel ends in 1 but secret bit is 0
						rgb--;
					}
				}
			}

			// Render new pixel with possibly modfied colour
			newImage.setRGB(x,y,rgb);
		}
		
		return newImage;
	}
	
	private void writeImage(BufferedImage image)
	throws IOException {
		ImageIO.write(image, "BMP", new File("ImageWithSecret.bmp"));
	}
	
	private boolean[] bytesToBits(byte[] bytes){
		boolean[] result = new boolean[bytes.length*8];
		int boolMarker = 0;		
		for(int byteMarker = 0; byteMarker < bytes.length; byteMarker++){
					
			// Get byte as binary string
			String binary = Integer.toBinaryString((int) 128 + bytes[byteMarker]);
			
			// Work out the amount of zero to prepend
			int zeroPad = 8 - binary.length();
			
			// Pad with zeros
			for(int i = 0; i < zeroPad; i++){
				binary = "0" + binary;
			}
			
			// Convert to boolean array
			for(int i = 0; i < binary.length(); i++, boolMarker++){
				if(binary.charAt(i)=='1'){
					result[boolMarker] = true;
				} else {
					result[boolMarker] = false;
				}
			}
		}
		return result;
	}
	
	private boolean[] fileSizeToBits(int size){
		
		boolean[] result = new boolean[31];
		
		// Get binary string
		String binary = Integer.toBinaryString(size);
		
		// Number of zero to pad with
		int zeroPad = 31 - binary.length();
		
		// Pad with zeros
		for(int i = 0; i < zeroPad; i++){
			binary = "0" + binary;
		}
		
		// Convert to boolean array			
		for(int i = 0; i < result.length; i++){
			if(binary.charAt(i)=='1'){
				result[i] = true;
			} else {
				result[i] = false;
			}
		}
		
		return result;
	}
	
	private byte[] openSecretFile(String secretPath)
	throws Exception, IOException{
		if(secretPath.length() > 255){
			System.out.println("Secret file name too long!");
			throw new Exception();
		}
		File file = new File(secretPath);
		byte[] buffer = new byte[(secretPath.length() + 1) + (int)file.length()]; // Allow room for file name
		FileInputStream fis = null;
		
		// Add file name size
		buffer[0] = (byte)secretPath.length(); //length of name
		
		// add file name
		for(int i = 0; i < secretPath.length(); i++){
			buffer[i+1] = (byte)secretPath.charAt(i);
		}
		
		fis = new FileInputStream(file);			
		for(int i  = secretPath.length() + 1; i < buffer.length; i++){			
			buffer[i] = (byte)fis.read();
		}
		fis.close();
		return buffer;
	}
	
	private BufferedImage loadImage(String imagePath)
	throws IOException{
		return ImageIO.read(new File(imagePath));		
	}
	
	private byte[] getPixels(BufferedImage image){
		return ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
	}
}
