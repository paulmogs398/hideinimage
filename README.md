# README #

A steganography application, hide a file in a image file's pixels in plain sight!

Inspired by: Secrets Hidden in Images (Steganography) - Computerphile
Please watch this first: https://www.youtube.com/watch?v=TWEXCYQKyDc

### Compile ###

	#@#:~/Documents/HideInImage$ javac *.java
	
### Saving a Sound Clip in an Image ###

	#@#:~/Documents/HideInImage$ java ConcealInImage SecretSong.mp3 radio.png 
	Welcome to HideInImage... Hiding SecretSong.mp3 in radio.png
	Bits in secret file: 643071
	Bytes in image file: 24064000
	Secret file can be hidden in image file.
	
	#@#:~/Documents/HideInImage$ rm SecretSong.mp3
	
	#@#:~/Documents/HideInImage$ java RevealFromImage ImageWithSecret.bmp 
	Welcome to Reveal From Image!
	Found hidden file: SecretSong.mp3
	Writting 80365B to file...

### Saving a Brief Document in a Tiny Image ###

	#@#:~/Documents/HideInImage$ java ConcealInImage SecretMessage.docx RandomPicture.jpg 
	Welcome to HideInImage... Hiding SecretMessage.docx in RandomPicture.jpg
	Bits in secret file: 265255
	Bytes in image file: 921600
	Secret file can be hidden in image file.
	
	#@#:~/Documents/HideInImage$ rm SecretMessage.docx
	
	#@#:~/Documents/HideInImage$ java RevealFromImage ImageWithSecret.bmp 
	Welcome to Reveal From Image!
	Found hidden file: SecretMessage.docx
	Writting 33134B to file...

If you have any problems, questions, comments or advice to improve this application please email paulmogs398@gmail.com  many thanks :D