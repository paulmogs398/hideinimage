import javax.imageio.*;
import java.io.File;
import java.awt.image.BufferedImage; 
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.util.Arrays;
import java.io.FileOutputStream;

public class RevealFromImage{
	
	
	public static void main(String[] args){
		System.out.println("Welcome to Reveal From Image!");
		if(args.length!=1){
			System.out.println("Please provide the path of the image to extact the hidden file from.");
		}
		
		new RevealFromImage(args[0]);
	}
	
	public RevealFromImage(String imagePath){
		
		byte[] pixels = new byte[0];
		boolean alpha = false;
		try{
			
			BufferedImage img = loadImage(imagePath);
			alpha = img.getAlphaRaster()!=null;
			pixels = getPixels(img);
		} catch(IOException ioe){
			System.out.println("Error loading image!");
			System.exit(1);
		}
		
		// Get data data from least most significant bits
		boolean[] secrets = getHiddenData(pixels,alpha);
		
		// Find the length and extract the meaningful portion
		secrets = extractReleventData(secrets);
		
		// Get the length of file name
		int nameLength = getFileNameLength(secrets);
		
		// Get the name
		String fileName = getName(Arrays.copyOfRange(secrets,8,8+(nameLength*8)));
		
		// Remove filename portion
		secrets = Arrays.copyOfRange(secrets,8+(nameLength*8),secrets.length);
		
		// Get file data
		byte[] secretsData = getFileBytes(secrets);
		
		// Inform user
		println("Found hidden file: " + fileName);
		println("Writting " + secretsData.length + "B to file...");
		
		// Write file
		try{
			writeFile(secretsData,fileName);
		} catch (Exception e){
			System.out.println("Error writting file!");
			System.exit(1);
		}
		
	}
	
	private void writeFile(byte[] data, String name)
	throws Exception{
		FileOutputStream writer = new FileOutputStream(name);
		writer.write(data);
		writer.close();
	}
	
	private byte[] getFileBytes(boolean[] data){
		byte[] result = new byte[data.length / 8];
		for(int i = 0, b = 0; i < data.length; i = i + 8, b++){
			result[b] = (byte)(getNumberFor(Arrays.copyOfRange(data,i,i+8))-128);
		}
		return result;
	}
	
	private String getName(boolean[] data){
		String result = "";
		for(int i = 0; i < data.length; i = i + 8){
			result = result + (char)(getNumberFor(Arrays.copyOfRange(data,i,i+8))-128);
		}
		return result;
	}
	
	private int getFileNameLength(boolean[] data){	
		return getNumberFor(Arrays.copyOfRange(data,0,8)) - 128;
	}
	
	private boolean[] extractReleventData(boolean[] data){
		
		// Get length of data from first 31 bits
		boolean[] dataLengthBits = Arrays.copyOfRange(data, 0, 31);
		
		// Convert boolean[] binary to number
		int dataLength = getNumberFor(dataLengthBits);
		
		// Get relevent data
		return Arrays.copyOfRange(data,31,dataLength+31);
	}
	
	private int getNumberFor(boolean[] data){
		int result = 0;
		for(int n = data.length-1, pos = 0; n >= 0; n--, pos++){
			if(data[pos]){
				result = result + ((int)Math.pow(2,n));
			}
		}
		return result;
	}
	
	private boolean[] getHiddenData(byte[] pixels, boolean hasAlphaChannel){
		boolean[] result = new boolean[pixels.length/(hasAlphaChannel?4:3)];		
		// Get each pixel color and write new image
		for (int pixel = 0, i = 0; pixel < pixels.length; pixel = pixel + (hasAlphaChannel?4:3), i++) {

			// Read pixel colour, if necessary ignoring alpha, byte order:  ?A (Alpha)?, B (Blue), G (Green), R (Red)
			int b = ( (int) pixels[pixel + (hasAlphaChannel?1:0)] & 255);
			int g = ( (int) pixels[pixel + (hasAlphaChannel?2:1)] & 255);
			int r = ( (int) pixels[pixel + (hasAlphaChannel?3:2)] & 255);
			
			// Construct RGB value
			int rgb = r;
			rgb = (rgb << 8) + g;
			rgb = (rgb << 8) + b;
			
			// Add bit to result
			String pixelBinary = Integer.toBinaryString(rgb);
			result[i] = (pixelBinary.charAt(pixelBinary.length()-1)=='1');
		}		
		return result;
	}
	
	private byte[] getPixels(BufferedImage image){
		return ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
	}
	
	private BufferedImage loadImage(String imagePath)
	throws IOException{
		return ImageIO.read(new File(imagePath));		
	}
	
	private void println(String message){
		System.out.println(message);
	}
	
	private void print(String message){
		System.out.print(message);
	}
	
}
